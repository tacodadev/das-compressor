# DAS Compressor

```
echo "abbcccc" | ruby gzip.rb compress
echo "abbcccc" | ruby gzip.rb compress | ruby gzip.rb decompress
cat gzip.rb | ruby gzip.rb compress
cat gzip.rb | ruby gzip.rb compress | ruby gzip.rb decompress
cat gzip.rb | ruby gzip.rb compress | ruby gzip.rb decompress | sha1sum
sha1sum gzip.rb
 ```
